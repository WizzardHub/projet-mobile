package me.wizzard.projetmobile.api.milo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.wizzard.projetmobile.api.milo.entity.Bilan;
import me.wizzard.projetmobile.api.milo.entity.Entreprise;
import me.wizzard.projetmobile.api.milo.entity.Etudiant;
import me.wizzard.projetmobile.api.milo.entity.Tuteur;
import me.wizzard.projetmobile.api.milo.entity.login.Login;
import me.wizzard.projetmobile.api.milo.modules.GsonDeserializer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MiloService {

    // server
    private final String BASE_URL = "http://10.0.2.2:5000";
    private final String PROD_URL = "http://192.168.0.175:5000";

    private final IMiloService iMiloService;

    public MiloService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Login.class, new GsonDeserializer<Login>())
                .registerTypeAdapter(Bilan.class, new GsonDeserializer<Bilan>())
                .registerTypeAdapter(Etudiant.class, new GsonDeserializer<Etudiant>())
                .registerTypeAdapter(Tuteur.class, new GsonDeserializer<Tuteur>())
                .registerTypeAdapter(Entreprise.class, new GsonDeserializer<Entreprise>())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        iMiloService = retrofit.create(IMiloService.class);
    }

    public IMiloService getiMiloService() {
        return iMiloService;
    }

}
