package me.wizzard.projetmobile.api.milo.entity;

import com.google.gson.annotations.SerializedName;

public class Bilan {

    @SerializedName("id")
    int id;

    @SerializedName("idBilan")
    int idBilan;

    @SerializedName("idEtudiant")
    int idEtudiant;

    @SerializedName("dateBilan")
    String dateBilan;

    @SerializedName("remarqueBilan")
    String remarqueBilan;

    @SerializedName("noteEntretientBilan")
    float noteEntretientBilan;

    @SerializedName("noteOralBilan")
    float noteOralBilan;

    @SerializedName("noteDossierBilan")
    float noteDossierBilan;

    @SerializedName("noteOraleFinBilan")
    float noteOraleFinBilan;

    @SerializedName("noteDossierFinBilan")
    float noteDossierFinBilan;

    public int getId() {
        return id;
    }

    public int getIdBilan() {
        return idBilan;
    }

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public String getDateBilan() {
        return dateBilan;
    }

    public float getNoteEntretientBilan() {
        return noteEntretientBilan;
    }

    public float getNoteOralBilan() {
        return noteOralBilan;
    }

    public float getNoteDossierBilan() {
        return noteDossierBilan;
    }

    public float getNoteOraleFinBilan() {
        return noteOraleFinBilan;
    }

    public float getNoteDossierFinBilan() {
        return noteDossierFinBilan;
    }

    public String getRemarqueBilan() {
        return remarqueBilan;
    }

    public Bilan(int id, int idBilan, int idEtudiant, String dateBilan, String remarqueBilan, float noteEntretientBilan, float noteOralBilan, float noteDossierBilan, float noteOraleFinBilan, float noteDossierFinBilan) {
        this.id = id;
        this.idBilan = idBilan;
        this.idEtudiant = idEtudiant;
        this.dateBilan = dateBilan;
        this.remarqueBilan = remarqueBilan;
        this.noteEntretientBilan = noteEntretientBilan;
        this.noteOralBilan = noteOralBilan;
        this.noteDossierBilan = noteDossierBilan;
        this.noteOraleFinBilan = noteOraleFinBilan;
        this.noteDossierFinBilan = noteDossierFinBilan;
    }
}
