package me.wizzard.projetmobile.api.milo.entity.login;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

public class Login {

    private String user;
    private String pass;

    public Login(String user, String pass) {
        this.user = user;
        this.pass = new String(Hex.encodeHex(DigestUtils.md5(pass))); // Hashing MD5
    }

}
