package me.wizzard.projetmobile.api.milo.entity.login;

public class LoginResponse {

    private boolean success;
    private short uid;

    public boolean isSuccess() {
        return success;
    }

    public short getUid() {
        return uid;
    }
}
