package me.wizzard.projetmobile.api.milo.entity;

import com.google.gson.annotations.SerializedName;

public class Etudiant {

    @SerializedName("id")
    int id;

    @SerializedName("idTuteur")
    int idTuteur;

    // gay

    @SerializedName("idEntreprise")
    int idEntreprise;

    @SerializedName("nom")
    String nom;

    @SerializedName("prenom")
    String prenom;

    @SerializedName("telephone")
    String telephone;

    @SerializedName("mail")
    String mail;

    @SerializedName("login")
    String login;

    @SerializedName("motDePasse")
    String motDePasse;

    @SerializedName("remarque")
    String remarque;

    @SerializedName("sujet")
    String sujet;

    @SerializedName("datePriseTelephone")
    String datePriseTelephone;

    public int getId() {
        return id;
    }

    public int getIdTuteur() {
        return idTuteur;
    }

    public int getIdEntreprise() {
        return idEntreprise;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getMail() {
        return mail;
    }

    public String getLogin() {
        return login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public String getRemarque() {
        return remarque;
    }

    public String getSujet() {
        return sujet;
    }

    public String getDatePriseTelephone() {
        return datePriseTelephone;
    }

    public Etudiant(int id, int idTuteur, int idEntreprise, String nom, String prenom, String telephone, String mail, String login, String motDePasse, String remarque, String sujet, String datePriseTelephone) {
        this.id = id;
        this.idTuteur = idTuteur;
        this.idEntreprise = idEntreprise;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.mail = mail;
        this.login = login;
        this.motDePasse = motDePasse;
        this.remarque = remarque;
        this.sujet = sujet;
        this.datePriseTelephone = datePriseTelephone;
    }
}
