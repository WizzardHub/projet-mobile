package me.wizzard.projetmobile.api.milo.entity;

import com.google.gson.annotations.SerializedName;

public class Entreprise {

    @SerializedName("id")
    private int id;

    @SerializedName("libelle")
    private String nom;

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public Entreprise(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
}
