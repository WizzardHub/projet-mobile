package me.wizzard.projetmobile.api.milo;

import java.util.List;

import me.wizzard.projetmobile.api.milo.entity.Bilan;
import me.wizzard.projetmobile.api.milo.entity.Entreprise;
import me.wizzard.projetmobile.api.milo.entity.Etudiant;
import me.wizzard.projetmobile.api.milo.entity.Tuteur;
import me.wizzard.projetmobile.api.milo.entity.login.Login;
import me.wizzard.projetmobile.api.milo.entity.login.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface IMiloService {

    @POST("/login")
    Call<LoginResponse> handleLogin(@Body Login login);

    @GET("/bilan")
    Call<List<Bilan>> getAllBilans();

    @GET("/etudiant")
    Call<List<Etudiant>> getAllEtudiants();

    @GET("/tuteur")
    Call<List<Tuteur>> getAllTuteurs();

    @GET("/entreprise")
    Call<List<Entreprise>> getAllEntreprises();

}
