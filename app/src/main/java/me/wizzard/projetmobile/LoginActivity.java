package me.wizzard.projetmobile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.wizzard.projetmobile.api.milo.IMiloService;
import me.wizzard.projetmobile.api.milo.MiloController;
import me.wizzard.projetmobile.api.milo.MiloService;
import me.wizzard.projetmobile.api.milo.entity.login.Login;
import me.wizzard.projetmobile.api.milo.entity.login.LoginResponse;
import me.wizzard.projetmobile.api.milo.modules.GsonDeserializer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private TextView tvLabelError;
    private EditText etLogin, etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tvLabelError = findViewById(R.id.tvLabelError);
        etLogin = findViewById(R.id.etLogin);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(v -> {
            Login payload = new Login(etLogin.getText().toString(), etPassword.getText().toString());

            MiloService ms = new MiloService();
            IMiloService iMiloService = ms.getiMiloService();

            Call<LoginResponse> call = iMiloService.handleLogin(payload);
            call.enqueue(new MiloController<LoginResponse>() {
                @Override
                public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {

                    if (response.isSuccessful()) {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                        Gson gson = new Gson();
                        String json = gson.toJson(response.body());
                        intent.putExtra("json", json);

                        startActivity(intent);
                    } else {

                        etLogin.setBackgroundTintList(
                                ColorStateList.valueOf(
                                        getResources().getColor(R.color.light_red)));

                        etPassword.setBackgroundTintList(
                                ColorStateList.valueOf(
                                        getResources().getColor(R.color.light_red)));

                        tvLabelError.setText(
                                getString(R.string.invalid_pw_activity_login));
                    }
                }
            });
        });
    }
}