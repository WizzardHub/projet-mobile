package me.wizzard.projetmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;

import com.google.gson.Gson;

import me.wizzard.projetmobile.api.milo.entity.Etudiant;
import me.wizzard.projetmobile.api.milo.entity.login.LoginResponse;
import me.wizzard.projetmobile.db.SQLiteManager;

public class Information extends AppCompatActivity {

    private SQLiteManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        manager = new SQLiteManager(this);

        int id = getIntent().getExtras().getShort("id");


        EditText nom = findViewById(R.id.etNom);
        EditText prenom = findViewById(R.id.etPrenom);
        EditText tel = findViewById(R.id.etTel);
        EditText mail = findViewById(R.id.etMail);
        EditText rem = findViewById(R.id.etRemarque);
        EditText date = findViewById(R.id.etDateEntretient);
        EditText sujet = findViewById(R.id.etSujet);

        Etudiant etudiant = manager.getEtudiantById(id);

        nom.setText(etudiant.getNom());
        prenom.setText(etudiant.getPrenom());
        tel.setText(etudiant.getTelephone());
        mail.setText(etudiant.getMail());
        rem.setText(etudiant.getRemarque());
        date.setText(etudiant.getDatePriseTelephone());
        sujet.setText(etudiant.getSujet());
    }
}