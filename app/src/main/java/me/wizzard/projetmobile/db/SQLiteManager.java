package me.wizzard.projetmobile.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.List;

import me.wizzard.projetmobile.api.milo.entity.Bilan;
import me.wizzard.projetmobile.api.milo.entity.Entreprise;
import me.wizzard.projetmobile.api.milo.entity.Etudiant;
import me.wizzard.projetmobile.api.milo.entity.Tuteur;

public class SQLiteManager extends SQLiteOpenHelper {

    private Context cxt;
    private SQLiteDatabase db;
    public static final String DB_NAME = "MiloV5.db";
    public static final int DB_VERSION = 1;

    public SQLiteManager(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        db = this.getWritableDatabase();
        cxt = context;
    }

    @Override
    public void onCreate(android.database.sqlite.SQLiteDatabase db) {
        /* Bilan Table */
        db.execSQL("create table Bilan (Id_Bil integer primary key autoincrement, Id_Num_Bil integer, Id_Etu_Bil integer, Dat_Bil text, Not_Ent_Bil float, Not_Ora_Bil float, Not_Dos_Bil float, Not_Dos_Fin_Bil float, Not_Ora_Fin_Bil float, Rem_Bil text)");
        /* Etudiant Table */
        db.execSQL("create table Etudiant (Id_Etu integer primary key autoincrement, Id_Tut int, Id_Ent int, Nom_Etu text, Pre_Etu text, Tel_Etu text, Mai_Etu text, Log_Etu text, Mdp_Etu text, Rem_Etu text, Dat_Pnt_Tel_Etu text, Suj_Etu text)");
        /* Tuteur Table */
        db.execSQL("create table Tuteur (Id_Tut integer primary key autoincrement, Nom_Tut text, Pre_Tut text, Mai_Tut text)");
        /* Entreprise Table */
        db.execSQL("create table Entreprise (Id_Ent integer primary key autoincrement, Lib_Ent text)");
    }

    @Override
    public void onUpgrade(android.database.sqlite.SQLiteDatabase db, int oldVersion, int newVersion) {
        /* Bilan Table */
        db.execSQL("drop table if exists Bilan");
        /* Etudiant Table */
        db.execSQL("drop table if exists Etudiant");
        /* Tuteur Table */
        db.execSQL("drop table if exists Tuteur");
        /* Entreprise Table */
        db.execSQL("drop table if exists Entreprise");
    }

    public void insertAllBilans(List<Bilan> bilans) {
        db.execSQL("delete from Bilan");
        bilans.forEach(b -> {
            ContentValues cv = new ContentValues();
            cv.put("Id_Bil", b.getId());
            cv.put("Id_Num_Bil", b.getIdBilan());
            cv.put("Id_Etu_Bil", b.getIdEtudiant());
            cv.put("Dat_Bil", b.getDateBilan());
            cv.put("Not_Ent_Bil", b.getNoteEntretientBilan());
            cv.put("Not_Ora_Bil", b.getNoteOralBilan());
            cv.put("Not_Dos_Bil", b.getNoteDossierBilan());
            cv.put("Not_Dos_Fin_Bil", b.getNoteDossierFinBilan());
            cv.put("Not_Ora_Fin_Bil", b.getNoteOraleFinBilan());
            cv.put("Rem_Bil", b.getRemarqueBilan());
            db.insert("Bilan", null, cv);
        });
    }

    public void insertBilan(Bilan b) {
        db.execSQL("delete from Bilan");
        ContentValues cv = new ContentValues();
        cv.put("Id_Bil", b.getId());
        cv.put("Id_Num_Bil", b.getIdBilan());
        cv.put("Id_Etu_Bil", b.getIdEtudiant());
        cv.put("Dat_Bil", b.getDateBilan());
        cv.put("Not_Ent_Bil", b.getNoteEntretientBilan());
        cv.put("Not_Ora_Bil", b.getNoteOralBilan());
        cv.put("Not_Dos_Bil", b.getNoteDossierBilan());
        cv.put("Not_Dos_Fin_Bil", b.getNoteDossierFinBilan());
        cv.put("Not_Ora_Fin_Bil", b.getNoteOraleFinBilan());
        cv.put("Rem_Bil", b.getRemarqueBilan());
        db.insert("Bilan", null, cv);
    }

    public Bilan getBilanById(int id, int idEdu) {
        Cursor cursor = db.rawQuery("select * from Bilan where Id_Num_Bil = ? and Id_Etu_Bil = ?", new String[] {String.valueOf(id), String.valueOf(idEdu)});
        Bilan bilan = null;

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            bilan = new Bilan(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getString(2),
                    cursor.getString(6),
                    cursor.getFloat(3),
                    cursor.getFloat(4),
                    cursor.getFloat(4),
                    cursor.getFloat(5),
                    cursor.getFloat(6)
                    );
        }

        return bilan;
    }

    public void insertAllEtudiants(List<Etudiant> etudiants) {
        db.execSQL("delete from Etudiant");
        etudiants.forEach(e -> {
            ContentValues cv = new ContentValues();
            cv.put("Id_Etu", e.getId());
            cv.put("Id_Tut", e.getIdTuteur());
            cv.put("Id_Ent", e.getIdEntreprise());
            cv.put("Nom_Etu", e.getNom());
            cv.put("Pre_Etu", e.getPrenom());
            cv.put("Tel_Etu", e.getTelephone());
            cv.put("Mai_Etu", e.getMail());
            cv.put("Log_Etu", e.getLogin());
            cv.put("Mdp_Etu", e.getMotDePasse());
            cv.put("Rem_Etu", e.getRemarque());
            cv.put("Dat_Pnt_Tel_Etu", e.getDatePriseTelephone());
            cv.put("Suj_Etu", e.getSujet());
            db.insert("Etudiant", null, cv);
        });
    }

    public Etudiant getEtudiantById(int id) {
        Cursor cursor = db.rawQuery("select * from Etudiant where Id_Etu = ?", new String[] {String.valueOf(id)});
        Etudiant etudiant = null;

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            etudiant = new Etudiant(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8),
                    cursor.getString(9),
                    cursor.getString(11),
                    cursor.getString(10)
            );
        }

        return etudiant;
    }

    public void insertAllTuteurs(List<Tuteur> tuteurs) {
        db.execSQL("delete from Tuteur");
        tuteurs.forEach(t -> {
            ContentValues cv = new ContentValues();
            cv.put("Id_Tut", t.getId());
            cv.put("Nom_Tut", t.getNom());
            cv.put("Pre_Tut", t.getPrenom());
            cv.put("Mai_Tut", t.getMail());
            db.insert("Tuteur", null, cv);
        });
    }

    public Tuteur getTuteurById(int id) {

        Cursor cursor = db.rawQuery("select * from Tuteur where Id_Num_Etu = ?", new String[] {String.valueOf(id)});
        Tuteur tuteur = null;

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            tuteur = new Tuteur(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );
        }

        return tuteur;
    }

    public void insertAllEntreprises(List<Entreprise> entreprises) {
        db.execSQL("delete from Entreprise");
        entreprises.forEach(e -> {
            ContentValues cv = new ContentValues();
            cv.put("Id_Ent", e.getId());
            cv.put("Lib_Ent", e.getNom());
            db.insert("Entreprise", null, cv);
        });
    }

    public Entreprise getEntrepriseById(int id) {
        Cursor cursor = db.rawQuery("select * from Entreprise where Id_Ent = ?", new String[] {String.valueOf(id)});
        Entreprise entreprise = null;

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            entreprise = new Entreprise(
                    cursor.getInt(0),
                    cursor.getString(1)
            );
        }

        return entreprise;
    }
}
