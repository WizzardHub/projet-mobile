package me.wizzard.projetmobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import me.wizzard.projetmobile.api.milo.IMiloService;
import me.wizzard.projetmobile.api.milo.MiloController;
import me.wizzard.projetmobile.api.milo.MiloService;
import me.wizzard.projetmobile.api.milo.entity.Bilan;
import me.wizzard.projetmobile.api.milo.entity.Entreprise;
import me.wizzard.projetmobile.api.milo.entity.Etudiant;
import me.wizzard.projetmobile.api.milo.entity.Tuteur;
import me.wizzard.projetmobile.api.milo.entity.login.LoginResponse;
import me.wizzard.projetmobile.db.SQLiteManager;
import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private List<Bilan> bilans;
    private List<Etudiant> etudiants;
    private List<Tuteur> tuteurs;
    private List<Entreprise> entreprises;

    private SQLiteManager manager;

    private Button btnInfo, btnNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gson gson = new Gson();
        manager = new SQLiteManager(this);

        LoginResponse res = gson
                .fromJson(getIntent().getStringExtra("json"), LoginResponse.class);

        bilans = new ArrayList<>();
        etudiants = new ArrayList<>();

        btnInfo = findViewById(R.id.btnInfo);
        btnNote = findViewById(R.id.btnNote);

        btnInfo.setVisibility(View.INVISIBLE);
        btnNote.setVisibility(View.INVISIBLE);

        /* Getting all data from Rest API */
        MiloService ms = new MiloService();
        IMiloService iMiloService = ms.getiMiloService();

        Call<List<Bilan>> callBilan = iMiloService.getAllBilans();
        callBilan.enqueue(new MiloController<List<Bilan>>() {
            @Override
            public void onResponse(@NonNull Call<List<Bilan>> call, @NonNull Response<List<Bilan>> response) {
                if (response.isSuccessful()) {
                    bilans = response.body();

                    // SQLite
                    manager.insertAllBilans(bilans);

                    btnNote.setVisibility(View.VISIBLE);
                    btnNote.setOnClickListener(v -> {
                        Intent intent = new Intent(MainActivity.this, Note.class);
                        Etudiant etu = etudiants.stream().filter(x -> x.getId() == res.getUid()).findFirst().orElse(null);
                        List<Bilan> bilan = bilans.stream().filter(x -> x.getIdEtudiant() == etu.getId()).collect(Collectors.toList());
                        String json = gson.toJson(bilan);
                        intent.putExtra("json", json);
                        startActivity(intent);
                    });
                } else {
                    System.out.println("Une erreur est survenue lors de la récupération des données des bilans.");
                }

            }
        });

        Call<List<Etudiant>> callEtudiant = iMiloService.getAllEtudiants();
        callEtudiant.enqueue(new MiloController<List<Etudiant>>() {
            @Override
            public void onResponse(@NonNull Call<List<Etudiant>> call, @NonNull Response<List<Etudiant>> response) {
                if (response.isSuccessful()) {
                    etudiants = response.body();

                    // SQLite
                    manager.insertAllEtudiants(etudiants);

                    btnInfo.setVisibility(View.VISIBLE);
                    btnInfo.setOnClickListener(v -> {
                        Intent intent = new Intent(MainActivity.this, Information.class);
                        intent.putExtra("id", res.getUid());
                        startActivity(intent);
                    });
                } else {
                    System.out.println("Une erreur est survenue lors de la récupération des données des etudiants.");
                }

            }
        });

        Call<List<Tuteur>> callTuteur = iMiloService.getAllTuteurs();
        callTuteur.enqueue(new MiloController<List<Tuteur>>() {
            @Override
            public void onResponse(@NonNull Call<List<Tuteur>> call, @NonNull Response<List<Tuteur>> response) {
                if (response.isSuccessful()) {
                    tuteurs = response.body();

                    // SQLite
                    manager.insertAllTuteurs(tuteurs);

                } else {
                    System.out.println("Une erreur est survenue lors de la récupération des données des tuteurs.");
                }
            }
        });

        Call<List<Entreprise>> callEntreprise = iMiloService.getAllEntreprises();
        callEntreprise.enqueue(new MiloController<List<Entreprise>>() {
            @Override
            public void onResponse(@NonNull Call<List<Entreprise>> call, @NonNull Response<List<Entreprise>> response) {
                if (response.isSuccessful()) {
                    entreprises = response.body();

                    // SQLite
                    manager.insertAllEntreprises(entreprises);
                } else {
                    System.out.println("Une erreur est survenue lors de la récupération des données des entreprise.");
                }
            }
        });
    }
}