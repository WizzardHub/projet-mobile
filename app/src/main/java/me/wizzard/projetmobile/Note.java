package me.wizzard.projetmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import me.wizzard.projetmobile.api.milo.entity.Bilan;
import me.wizzard.projetmobile.api.milo.entity.Etudiant;

public class Note extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        Gson gson = new Gson();
        Bilan[] bilansArray = gson
                .fromJson(getIntent().getStringExtra("json"), Bilan[].class);

        List<Bilan> bilans = Arrays.asList(bilansArray);

        Bilan bilan1 = bilans.stream().filter(x -> x.getIdBilan() == 1).findFirst().orElse(null);
        Bilan bilan2 = bilans.stream().filter(x -> x.getIdBilan() == 2).findFirst().orElse(null);

        EditText noteBilan1 = findViewById(R.id.etNoteBilan1);
        EditText dateBilan1 = findViewById(R.id.etDateBilan1);
        EditText noteBilan2 = findViewById(R.id.etNoteBilan2);
        EditText dateBilan2 = findViewById(R.id.etDateBilan2);

        float note1 = (bilan1.getNoteDossierBilan() + bilan1.getNoteOralBilan() + bilan1.getNoteEntretientBilan() + bilan1.getNoteDossierFinBilan() + bilan1.getNoteOraleFinBilan()) / 5F;
        float note2 = (bilan2.getNoteDossierBilan() + bilan2.getNoteOralBilan() + bilan2.getNoteEntretientBilan() + bilan2.getNoteDossierFinBilan() + bilan2.getNoteOraleFinBilan()) / 5F;

        noteBilan1.setText(String.valueOf(note1));
        dateBilan1.setText(bilan1.getDateBilan());
        noteBilan2.setText(String.valueOf(note2));
        dateBilan2.setText(bilan2.getDateBilan());
    }
}